﻿using System;
using System.Collections.Generic;

namespace CashRegister
{
    public interface IFiscalRegisterDriver
    {
        /// <summary>
        /// Запустить ФР
        /// </summary>
        /// <param name="settings"></param>
        void Start(FiscalRegisterSettings settings);

        /// <summary>
        /// Остановить ФР
        /// </summary>
        void Stop();

        /// <summary>
        /// Получить состояние ФР
        /// </summary>
        /// <returns></returns>
        CashRegisterResult BuildCashResult();

        /// <summary>
        /// Установить имя кассира
        /// </summary>
        /// <param name="name">имя кассира</param>
        void SetCashier(string name);

        /// <summary>
        /// Напечатать X-отчет
        /// </summary>
        void XReport();

        /// <summary>
        /// Напечатать Z-отчет
        /// </summary>
        void ZReport();

        /// <summary>
        /// Запросить информацию о поддерживаемых возможностях ФР
        /// </summary>
        /// <returns></returns>
        QueryInfoResult QueryInfo();

        /// <summary>
        /// Напечатать электронный журнал за последнюю смену
        /// </summary>
        void PrintElectronicJournalByLastSession();

        /// <summary>
        /// Получить электронный журнал за последнюю смену
        /// </summary>
        List<string> ElectronicJournalByLastSession();

        /// <summary>
        /// Изъять деньги из кассу
        /// </summary>
        /// <param name="sum"></param>
        void PayOut(decimal sum);

        /// <summary>
        /// Внести деньги в кассу
        /// </summary>
        /// <param name="sum"></param>
        void PayIn(decimal sum);

        /// <summary>
        /// Напечатать текстовый (нефискальный) документ
        /// </summary>
        /// <param name="lines"></param>
        void PrintText(List<LineInfo> lines);


        /// <summary>
        /// Напечатать чек продажи или возврата
        /// </summary>
        /// <param name="chequeTask"></param>
        void PrintCheque(ChequeTask chequeTask);

        /// <summary>
        /// Очистить текст на экране покупателя и показать текст приветствия через промежуток времени timespan. 
        /// Если после этой команды будет вызвана любая другая команда, это очистка текста отменяется
        /// </summary>
        /// <param name="timespan"></param>
        void CustomerDisplayIdleTask(TimeSpan timespan);

        /// <summary>
        /// Показать текст на экране покупателя
        /// </summary>
        /// <param name="timeToShow">время показа текста, после которого должен быть показан текст приветствия</param>
        /// <param name="topLeft"></param>
        /// <param name="topRight"></param>
        /// <param name="bottonLeft"></param>
        /// <param name="bottomRight"></param>
        void CustomerDisplayTextTask(TimeSpan timeToShow, string topLeft, string topRight, string bottonLeft,
            string bottomRight);

        /// <summary>
        /// Получить состояние денежного ящика, подключенного к ФР
        /// </summary>
        /// <returns></returns>
        CashDrawerStatus GetCashDrawerStatus();

        /// <summary>
        /// Открыть денежный ящик
        /// </summary>
        void OpenCashDrawer();

        /// <summary>
        /// Запущен ли ФР
        /// </summary>
        bool Started { get; }

        /// <summary>
        /// Текущее имя кассира
        /// </summary>
        string CashierName { get; }
    }
}
