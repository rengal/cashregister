﻿namespace CashRegister
{
    public class FiscalRegisterSettings
    {
        /// <summary>
        /// Интерфейс подключения
        /// </summary>
        public ConnectionInterface ConnectionInterface;

        /// <summary>
        /// Номер порта
        /// </summary>
        public int PortNumber;

        /// <summary>
        /// Скорость подключения по последовательному порту
        /// </summary>
        public int BaudRate;

        /// <summary>
        /// Адрес ФР
        /// </summary>
        public string HostAddress;
        
        /// <summary>
        /// Порт
        /// </summary>
        public int HostPort;

        /// <summary>
        /// Ширина строки при использовании основного шрифта f0
        /// </summary>
        public int Font0Width;

        /// <summary>
        /// Номер или название кодовой страницы (например, "windows-1251", "866")
        /// </summary>
        public string CodePage;

        /// <summary>
        /// Настройки экрана покупателя, подключенного к ФР
        /// </summary>
        public InternalCustomerDisplaySettings CustomerDisplaySettings;
    }
}
