﻿namespace CashRegister
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnBuildCashResult = new System.Windows.Forms.Button();
            this.tbOutput = new System.Windows.Forms.RichTextBox();
            this.lbOutput = new System.Windows.Forms.Label();
            this.btnXReport = new System.Windows.Forms.Button();
            this.btnZReport = new System.Windows.Forms.Button();
            this.btnPrintText = new System.Windows.Forms.Button();
            this.btnPrintCheque = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 26);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(93, 26);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnBuildCashResult
            // 
            this.btnBuildCashResult.Location = new System.Drawing.Point(190, 26);
            this.btnBuildCashResult.Name = "btnBuildCashResult";
            this.btnBuildCashResult.Size = new System.Drawing.Size(109, 23);
            this.btnBuildCashResult.TabIndex = 2;
            this.btnBuildCashResult.Text = "BuildCashResult";
            this.btnBuildCashResult.UseVisualStyleBackColor = true;
            this.btnBuildCashResult.Click += new System.EventHandler(this.btnBuildCashResult_Click);
            // 
            // tbOutput
            // 
            this.tbOutput.Location = new System.Drawing.Point(12, 205);
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.Size = new System.Drawing.Size(628, 350);
            this.tbOutput.TabIndex = 3;
            this.tbOutput.Text = "";
            // 
            // lbOutput
            // 
            this.lbOutput.AutoSize = true;
            this.lbOutput.Location = new System.Drawing.Point(12, 189);
            this.lbOutput.Name = "lbOutput";
            this.lbOutput.Size = new System.Drawing.Size(42, 13);
            this.lbOutput.TabIndex = 4;
            this.lbOutput.Text = "Output:";
            // 
            // btnXReport
            // 
            this.btnXReport.Location = new System.Drawing.Point(12, 72);
            this.btnXReport.Name = "btnXReport";
            this.btnXReport.Size = new System.Drawing.Size(75, 23);
            this.btnXReport.TabIndex = 5;
            this.btnXReport.Text = "XReport";
            this.btnXReport.UseVisualStyleBackColor = true;
            this.btnXReport.Click += new System.EventHandler(this.btnXReport_Click);
            // 
            // btnZReport
            // 
            this.btnZReport.Location = new System.Drawing.Point(93, 72);
            this.btnZReport.Name = "btnZReport";
            this.btnZReport.Size = new System.Drawing.Size(75, 23);
            this.btnZReport.TabIndex = 6;
            this.btnZReport.Text = "ZReport";
            this.btnZReport.UseVisualStyleBackColor = true;
            this.btnZReport.Click += new System.EventHandler(this.btnZReport_Click);
            // 
            // btnPrintText
            // 
            this.btnPrintText.Location = new System.Drawing.Point(12, 122);
            this.btnPrintText.Name = "btnPrintText";
            this.btnPrintText.Size = new System.Drawing.Size(75, 23);
            this.btnPrintText.TabIndex = 7;
            this.btnPrintText.Text = "PrintText";
            this.btnPrintText.UseVisualStyleBackColor = true;
            this.btnPrintText.Click += new System.EventHandler(this.btnPrintText_Click);
            // 
            // btnPrintCheque
            // 
            this.btnPrintCheque.Location = new System.Drawing.Point(93, 122);
            this.btnPrintCheque.Name = "btnPrintCheque";
            this.btnPrintCheque.Size = new System.Drawing.Size(75, 23);
            this.btnPrintCheque.TabIndex = 8;
            this.btnPrintCheque.Text = "PrintCheque";
            this.btnPrintCheque.UseVisualStyleBackColor = true;
            this.btnPrintCheque.Click += new System.EventHandler(this.btnPrintCheque_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 567);
            this.Controls.Add(this.btnPrintCheque);
            this.Controls.Add(this.btnPrintText);
            this.Controls.Add(this.btnZReport);
            this.Controls.Add(this.btnXReport);
            this.Controls.Add(this.lbOutput);
            this.Controls.Add(this.tbOutput);
            this.Controls.Add(this.btnBuildCashResult);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnBuildCashResult;
        private System.Windows.Forms.RichTextBox tbOutput;
        private System.Windows.Forms.Label lbOutput;
        private System.Windows.Forms.Button btnXReport;
        private System.Windows.Forms.Button btnZReport;
        private System.Windows.Forms.Button btnPrintText;
        private System.Windows.Forms.Button btnPrintCheque;
    }
}

