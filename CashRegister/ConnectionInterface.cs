﻿using System;

// ReSharper disable InconsistentNaming
namespace CashRegister
{
    public class ConnectionInterface
    {
        public static readonly ConnectionInterface COM = new ConnectionInterface("COM", "COM");
        public static readonly ConnectionInterface ETHERNET = new ConnectionInterface("ETHERNET", "ETHERNET");

        private readonly string name;
        private readonly string __value;

        private ConnectionInterface(string __value, string name)
        {
            this.__value = __value;
            this.name = name;
        }

        public static ConnectionInterface Parse(string value)
        {
            switch (value)
            {
                case "COM": return COM;
                case "ETHERNET": return ETHERNET;
                default: throw new ArgumentException("Undefined enum constant:" + value);
            }
        }

        public static ConnectionInterface[] VALUES
        {
            get
            {
                return new[]
                {
                    COM, 
                    ETHERNET
                };
            }
        }

        public string _Value
        {
            get { return __value; }
        }

        public override string ToString()
        {
            return this.__value;
        }

        public string Name
        {
            get { return name; }
        }
    }
}
// ReSharper restore InconsistentNaming