﻿using System.Collections.Generic;

namespace CashRegister
{
    public class LineInfo
    {
        /// <summary>
        /// Шрифт
        /// </summary>
        public TextFont Font;

        /// <summary>
        /// Допустимые атрибуты и значения (только для qr-кода)
        /// size: "tiny", "small", "normal", "large", "extralarge"
        /// correction: "low", "medium", "high", "ultra"
        /// Если атрибут отсутствует, то значение по умолчанию: size="normal", correction="medium"
        /// </summary>
        public KeyValuePair<string, string> Attributes;

        /// <summary>
        /// Содержание строки, штрих-кода или qr-кода
        /// </summary>
        public string Text;

        public LineInfo(TextFont font, KeyValuePair<string, string> attributes, string text)
        {
            Font = font;
            Attributes = attributes;
            Text = text;
        }
    }
}
