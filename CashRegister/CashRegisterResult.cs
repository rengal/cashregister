﻿namespace CashRegister
{
    public class CashRegisterResult
    {
        /// <summary>
        /// Сумма наличности в кассе
        /// </summary>
        public decimal CashSum;

        /// <summary>
        /// Удалось ли считать сумму наличности в кассе. Должно быть true
        /// </summary>
        public bool HasCashSum;

        /// <summary>
        /// Сумма выручки за смену. Равна сумме продаж минус сумме возвратов за смену
        /// </summary>
        public decimal TotalIncomeSum;

        /// <summary>
        /// Удалось ли считать сумму выручки за смену.
        /// </summary>
        public bool HasTotalIncomeSum;

        /// <summary>
        /// Номер последнего Z-отчета (номер смены)
        /// </summary>
        public int SessionNumber;

        /// <summary>
        /// Удалось ли считать номер последнего Z-отчета.
        /// </summary>
        public bool HasSessionNumber;

        /// <summary>
        /// Серийный номер ФР
        /// </summary>
        public string SerialNumber;

        /// <summary>
        /// Удалось ли считать серийный номер
        /// </summary>
        public bool HasSerial;

        /// <summary>
        /// Номер последнего документа
        /// </summary>
        public int DocumentNumber;

        /// <summary>
        /// Удалось ли считать номер последнего документа
        /// </summary>
        public bool HasDocument;
    }
}
