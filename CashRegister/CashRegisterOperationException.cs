﻿using System;

namespace CashRegister
{
    public enum OperationError
    {
        /// <summary>
        /// Некорректные настройки ФР
        /// </summary>
        ErrorConfiguration,

        /// <summary>
        /// Нет связи с ФР
        /// </summary>
        ErrorConnection,

        /// <summary>
        /// Некорректный ответ от ФР
        /// </summary>
        ErrorIncorrectResponse,

        /// <summary>
        /// Недостаточно средств в кассе для совершения операции
        /// </summary>
        ErrorNoMoney,

        /// <summary>
        /// Неправильный пароль доступа
        /// </summary>
        ErrorBadPassword,

        /// <summary>
        /// Некорректное значение номера отделения
        /// </summary>
        ErrorBadDepartment,

        /// <summary>
        /// Переполнение буфера чека
        /// </summary>
        ErrorChequeBufferOverflow,

        /// <summary>
        /// Некорректный идентификатор оплаты
        /// </summary>
        ErrorBadPaymentId,

        /// <summary>
        /// ФР не поддерживает печать произвольного нефискального документа
        /// </summary>
        ErrorPrintTextNotSupported,

        /// <summary>
        /// Некорректное значение налоговой ставки
        /// </summary>
        ErorVatNotSupported,

        /// <summary>
        /// ФР не поддерживает печать чека возврата
        /// </summary>
        ErrorRefundNotSupported,

        /// <summary>
        /// Нет бумаги
        /// </summary>
        ErrorNoPaper,

        /// <summary>
        /// Печать контрольной ленты не поддерживается
        /// </summary>
        ErrorPrintCrNotSupported,

        /// <summary>
        /// Запрос контрольной ленты не поддерживается
        /// </summary>
        ErrorQueryCrNotSupported,

        /// <summary>
        /// Истекла смена 24 часа
        /// </summary>
        Error24H,

        /// <summary>
        /// Другая ошибка
        /// </summary>
        ErrorOther
    }

    public class CashRegisterOperationException : Exception
    {
        public readonly OperationError OperationError;
        public readonly int AdditionalErrorCode;

        public CashRegisterOperationException(OperationError operationError, int additionalErrorCode, string message)
            : base(message)
        {
            OperationError = operationError;
            AdditionalErrorCode = additionalErrorCode;
        }
    }
}
