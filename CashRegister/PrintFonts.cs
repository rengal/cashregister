﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashRegister
{
    public enum TextFont
    {
        /// <summary>
        /// Основной шрифт
        /// </summary>
        F0,

        /// <summary>
        /// Шрифт двойной ширины
        /// </summary>
        F1,

        /// <summary>
        /// Двойная ширина и высота
        /// </summary>
        F2,

        /// <summary>
        /// Штрих-код
        /// </summary>
        Barcode,

        /// <summary>
        /// Qr-код
        /// </summary>
        QrCode,

        /// <summary>
        /// Отрезка
        /// </summary>
        Pagecut,

        /// <summary>
        /// Сигнал
        /// </summary>
        Bell,
    }
}
