﻿namespace CashRegister
{
    public class ChequeSale
    {
        /// <summary>
        /// Артикул товара
        /// </summary>
        public string Code;

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name;

        /// <summary>
        /// Цена без учета скидки
        /// </summary>
        public decimal Price;

        /// <summary>
        /// Количество
        /// </summary>
        public decimal Amount;

        /// <summary>
        /// Номер отделения
        /// </summary>
        public int Department;

        /// <summary>
        /// Процент налоговой ставки
        /// </summary>
        public decimal NdsPercent;

        /// <summary>
        /// Процент скидки
        /// </summary>
        public decimal DiscountPercent;

        /// <summary>
        /// Процент надбавки
        /// </summary>
        public decimal SubchargePercent;

        /// <summary>
        /// Сумма скидки
        /// </summary>
        public decimal DiscountSum;

        /// <summary>
        /// Сумма надбавки
        /// </summary>
        public decimal SubchargeSum;

        /// <summary>
        /// Стоимость товара (у учетом скидки на позицию и на весь чек)
        /// </summary>
        public decimal Sum;
    }
}
