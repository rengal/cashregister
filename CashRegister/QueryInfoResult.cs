﻿using System.Collections.Generic;

namespace CashRegister
{
    public class QueryInfoResult
    {
        /// <summary>
        /// Список безналичных типов оплаты. 
        /// Первый элемент пары - идентификатор типа оплаты (например, "A","B","C",.."1","2","3"). Передается в драйвер при печати чека.
        /// Второй элемент пары - текстовое описание типа оплаты, видно пользователю при настройке ФР, 
        /// на экране где нужно установить соответствие между типами платежей iiko и ФР
        /// </summary>
        public List<KeyValuePair<string, string>> PaymentTypes;

        /// <summary>
        /// Список налоговых ставок
        /// Первый элемент пары - идентификатор (номер) налоговой ставки
        /// Второй элемент пары - налоговая ставка в процентах. Специальное значение: -1 (не облагается НДС)
        /// </summary>
        public List<KeyValuePair<int, decimal>> Taxes;

        /// <summary>
        /// Поддерживает ли запрос контрольной ленты за последнюю смену для просмотра на экране
        /// </summary>
        public bool CapQueryElectronicJournalByLastSession;

        /// <summary>
        /// Поддерживает ли печать контрольной ленты за последнюю смену на ФР 
        /// </summary>
        public bool CapPrintElectronicJournalByLastSession;
    }
}
