﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CashRegister
{
    public partial class Form1 : Form
    {
        private readonly FiscalRegisterSettings fiscalRegisterSettings1 = new FiscalRegisterSettings
            {
                ConnectionInterface = ConnectionInterface.COM,
                PortNumber = 1,
                BaudRate = 9600
            };

        private readonly KeyValuePair<string, string> emptyAttr = default(KeyValuePair<string, string>);

        private readonly IFiscalRegisterDriver driver = CreateDriver();

        public Form1()
        {
            InitializeComponent();
        }

        private static IFiscalRegisterDriver CreateDriver()
        {
            return new MockDriver.MockDriver();
        }

        private void ExecuteOperation(Action action, string description)
        {
            try
            {
                LogLine(description);
                action();
                LogLine("Done");
            }
            catch (CashRegisterOperationException e)
            {
                LogLine("Operation failed with result: additionalCode: {1}, message: {2}", e.OperationError,
                    e.AdditionalErrorCode, e.Message);
            }
            LogLine(string.Empty);
        }

        private void LogLine(string format, params object[] args)
        {
            tbOutput.AppendText(string.Format(format, args) + Environment.NewLine);
        }

        #region IFiscalRegisterDriver methods

        private void btnStart_Click(object sender, EventArgs e)
        {
            ExecuteOperation(() => driver.Start(fiscalRegisterSettings1), "Start...");
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            ExecuteOperation(() => driver.Stop(), "Stop...");
        }

        private void btnBuildCashResult_Click(object sender, EventArgs e)
        {
            ExecuteOperation(() =>
                {
                    var result = driver.BuildCashResult();
                    LogLine("CashSum={0}, TotalIncomeSum={1}, SessionNumber={2}, SerialNumber={3}, DocumentNumber={4}",
                        result.CashSum, result.TotalIncomeSum, result.SessionNumber, result.SerialNumber,
                        result.DocumentNumber);
                }, "BuildCashResult...");
            driver.BuildCashResult();
        }

        private void btnXReport_Click(object sender, EventArgs e)
        {
            ExecuteOperation(() => driver.XReport(), "XReport...");
        }

        private void btnZReport_Click(object sender, EventArgs e)
        {
            ExecuteOperation(() => driver.ZReport(), "ZReport...");
        }

        private void btnPrintText_Click(object sender, EventArgs e)
        {
            ExecuteOperation(() =>
                {
                    var lines = new List<LineInfo>
                        {
                            new LineInfo(TextFont.F0, emptyAttr, "line font f0"),
                            new LineInfo(TextFont.F1, emptyAttr, "line font f1"),
                            new LineInfo(TextFont.F2, emptyAttr, "line font f2"),
                            new LineInfo(TextFont.Pagecut, emptyAttr, ""),
                            new LineInfo(TextFont.F0, emptyAttr, "line font f0 on second page"),
                            new LineInfo(TextFont.F1, emptyAttr, "line font f1 on second page"),
                            new LineInfo(TextFont.F2, emptyAttr, "line font f2 on second page"),
                            new LineInfo(TextFont.Barcode, emptyAttr, "1234567890128")
                        };
                    driver.PrintText(lines);
                }, "PrintText...");
        }

        private void btnPrintCheque_Click(object sender, EventArgs e)
        {
            ExecuteOperation(() =>
                {
                    var chequeSales = new List<ChequeSale>
                        {
                            new ChequeSale
                                {
                                    Amount = 2.0m,
                                    Code = "11001",
                                    Department = 1,
                                    Name = "Cola",
                                    Price = 50.0m,
                                    Sum = 100.0m
                                },
                            new ChequeSale
                                {
                                    Amount = 1.0m,
                                    Code = "11002",
                                    Department = 1,
                                    Name = "Hamburger",
                                    Price = 60.0m,
                                    Sum = 60.0m
                                }
                        };

                    var headerText = new List<LineInfo>
                        {
                            new LineInfo(TextFont.F0, emptyAttr, "extra text in header line1"),
                            new LineInfo(TextFont.F0, emptyAttr, "extra text in header line2")
                        };

                    var footerText = new List<LineInfo>
                        {
                            new LineInfo(TextFont.F0, emptyAttr, "Please visit our web site"),
                            new LineInfo(TextFont.QrCode, emptyAttr, "http://google.com")
                        };

                    var chequeTask = new ChequeTask
                        {
                            CashPayment = 160.0m,
                            IsRefund = false,
                            OrderInfo = "Table: 4",
                            PrintNds = false,
                            Sales = chequeSales,
                            OrderNum = 54321,
                            HeaderText = headerText,
                            FooterText = footerText
                        };
                    driver.PrintCheque(chequeTask);
                }, "PrintCheque...");
        }

        #endregion
    }
}
