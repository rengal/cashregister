﻿using System.Collections.Generic;

namespace CashRegister
{
    public class ChequeTask
    {
        /// <summary>
        /// Список позиций в чеке
        /// </summary>
        public List<ChequeSale> Sales;

        /// <summary>
        /// Сумма оплаты наличными
        /// </summary>
        public decimal CashPayment;

        /// <summary>
        /// Список безналичных оплат
        /// </summary>
        public List<ChequeCardPayment> СardPayments;

        /// <summary>
        /// Процент скидки на весь чек
        /// </summary>
        public decimal DiscountPercent;

        /// <summary>
        /// Процент надбавки на весь чек
        /// </summary>
        public decimal SubchargePercent;

        /// <summary>
        /// Сумма скидки на весь чек
        /// </summary>
        public decimal DiscountSum;

        /// <summary>
        /// Сумма надбавки на весь чек
        /// </summary>
        public decimal SubchargeSum;

        /// <summary>
        /// Является ли чеков возврата
        /// </summary>
        public bool IsRefund;

        /// <summary>
        /// Дополнительная строка с номером заказа и номером стола, обычно печатается после открытия чека
        /// </summary>
        public string OrderInfo;

        /// <summary>
        /// Номер заказа
        /// </summary>
        public int OrderNum;

        /// <summary>
        /// Следует ли печатать НДС в чеке
        /// </summary>
        public bool PrintNds;

        /// <summary>
        /// Дополнительный текст в начале чека
        /// </summary>
        public List<LineInfo> HeaderText;

        /// <summary>
        /// Дополнительный текст в конце чека
        /// </summary>
        public List<LineInfo> FooterText;
    }
}
