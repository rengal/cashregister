﻿namespace CashRegister
{
    public struct InternalCustomerDisplaySettings
    {
        /// <summary>
        /// Активен ли экран покупателя
        /// </summary>
        public bool IsEnabled;

        /// <summary>
        /// Текст ожидания, например "Добро пожаловать"
        /// </summary>
        public string IdleText;

        /// <summary>
        /// Число символов в строке
        /// </summary>
        public int Chars;

        /// <summary>
        /// Число строк
        /// </summary>
        public int Rows;
    }
}
