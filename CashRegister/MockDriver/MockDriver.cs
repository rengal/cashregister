﻿using System;
using System.Collections.Generic;

namespace CashRegister.MockDriver
{
    public class MockDriver : IFiscalRegisterDriver
    {
        #region Private Fields

        private bool started;
        private string cashierName;

        #endregion

        #region IFiscalRegisterDriver implementation

        public void Start(FiscalRegisterSettings settings)
        {
            started = true;
        }

        public void Stop()
        {
            started = false;
        }

        public CashRegisterResult BuildCashResult()
        {
            return new CashRegisterResult();
        }

        /// <summary>
        /// Установить имя кассира
        /// </summary>
        /// <param name="name">имя кассира</param>
        public void SetCashier(string name)
        {
            cashierName = name;
        }

        public void XReport()
        {
        }

        public void ZReport()
        {
        }

        public QueryInfoResult QueryInfo()
        {
            return new QueryInfoResult();
        }

        public void PrintElectronicJournalByLastSession()
        {
        }

        public List<string> ElectronicJournalByLastSession()
        {
            return new List<string>();
        }

        public void PayOut(decimal sum)
        {
        }

        public void PayIn(decimal sum)
        {
        }

        public void PrintText(List<LineInfo> lines)
        {
        }


        public void PrintCheque(ChequeTask chequeTask)
        {
        }

        public void CustomerDisplayIdleTask(TimeSpan timespan)
        {
        }

        public void CustomerDisplayTextTask(TimeSpan timeToShow, string topLeft, string topRight, string bottonLeft,
            string bottomRight)
        {
        }

        public CashDrawerStatus GetCashDrawerStatus()
        {
            return new CashDrawerStatus();
        }

        public void OpenCashDrawer()
        {
        }

        public bool Started
        {
            get { return started; }
        }

        public string CashierName
        {
            get { return cashierName; }
        }
    }

    #endregion
}
