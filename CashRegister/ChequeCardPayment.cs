﻿namespace CashRegister
{
    public class ChequeCardPayment
    {
        /// <summary>
        /// Использовать ли тип оплаты по умолчанию
        /// </summary>
        public bool DefaultPaymentId;

        /// <summary>
        /// Идентификатор оплаты, актуален если DefaultPaymentId=false;
        /// </summary>
        public string PaymentId;

        /// <summary>
        /// Сумма оплаты
        /// </summary>
        public decimal Sum;

        //Комментарий к оплате (например, номер карты)
        public string Comment;
    }
}